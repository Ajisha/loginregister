-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2018 at 12:12 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nexus`
--

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `reg_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`reg_id`, `name`, `email`, `password`) VALUES
(1, 'anu', 'anu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'aa', 'aa@gmmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'aa', 'aaq@gmmail.com', '0cc175b9c0f1b6a831c399e269772661'),
(4, 'ajisha', 'ajisha@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(5, 'sruthi', 'sruthimk353@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'aji', 'aji@gmail.com', 'acc440448ad4b7c5c5d315018e18a75f'),
(7, 'zzz', 'anua@gmail.com', '123456'),
(8, 'zzz', 'anua@gmail.com', '123456'),
(9, 'zzz', 'anua@gmail.com', '123456'),
(10, 'anu', 'anu@gmail.com', '123456'),
(11, 'anu', 'b@gmail.com', '123456'),
(12, 'q', 'q@gmail.com', '123456'),
(13, 'bb', 'bb@gmail.com', '123456'),
(14, 'c', 'c@gmail.com', '123456'),
(15, 'v', 'v@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`reg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
